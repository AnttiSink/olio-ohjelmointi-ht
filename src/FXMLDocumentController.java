// CT60A2411 Olio-ohjelmointi
// Syksy 2016
// Harjoitustyö
// Antti Sinkkonen 0439700 & Aleksi Rantamäki 0451458


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.web.WebView;

/**
 *
 * @author Antti
 */
public class FXMLDocumentController implements Initializable {
    
    
    private Label label;
    @FXML
    private ComboBox<String> spCombo;
    @FXML
    private Button mapButton;
    @FXML
    private WebView webViewArea;
    @FXML
    private ComboBox<Item> itemBox;
    @FXML
    private RadioButton firstClassButton;
    @FXML
    private RadioButton secondClassButton;
    @FXML
    private RadioButton thirdClassButton;
    @FXML
    private TextField nameField;
    @FXML
    private TextField sizeField;
    @FXML
    private TextField weightField;
    @FXML
    private CheckBox fragileCheck;
    @FXML
    private ComboBox<String> departureBox;
    @FXML
    private ComboBox<Smartpost> departureSmartPost;
    @FXML
    private ComboBox<String> destinationBox;
    @FXML
    private ComboBox<Smartpost> destinationSmartPost;
    
    // initialize the classes outside methods to make sure they can be used anywhere in the controller
    private Smartposts smartposts = Smartposts.getInstance();
    private Storage storage = Storage.getInstance(); // Tämä ei ole missään käytössä vielä
    private Items items = Items.getInstance();
    @FXML
    private Button removeButton;
    @FXML
    private ToggleGroup packageClass;
    @FXML
    private Button addItemButton;
    @FXML
    private Button createPackageButton;
//    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

            webViewArea.getEngine().load(getClass().getResource("index.html").toExternalForm()); // launch the webview  for map
            spCombo.getItems().addAll(smartposts.getCities()); // add cities to the combobox in the first tab
            
            // add basic items to the item combobox
            itemBox.getItems().addAll(items.getItemList());
            
            // add cities to departure and destination comboboxes
            departureBox.getItems().addAll(smartposts.getCities());
            destinationBox.getItems().addAll(smartposts.getCities());
            
            // add smartposts to departure and destination comboboxes
            departureSmartPost.getItems().addAll(smartposts.getSmartPosts());
            destinationSmartPost.getItems().addAll(smartposts.getSmartPosts());
    } // end of method    

    @FXML
    private void addSmartPosts(ActionEvent event) { // add smartposts on to map

        String city = spCombo.getValue();

        for (int i = 0; i < smartposts.getSmartPosts().size(); i++) {
            if (smartposts.getSmartPosts().get(i).getCity().equals(city)) {
                Smartpost sp = smartposts.getSmartPosts().get(i);
                String address = sp.getAddress() + "," + sp.getCode() + " " + sp.getCity();
                String info = sp.getPostOffice() + " " + sp.getAvailability();

                webViewArea.getEngine().executeScript("document.goToLocation('"+ address +"', '" + info + "', 'red')"); 
            }
        }
    }


    @FXML
    private void removeSmartposts(ActionEvent event) {
        
        webViewArea.getEngine().executeScript("document.deletePaths()"); // delete the smartposts from the map
    }

    @FXML
    private void filterDepList(MouseEvent event) { // filters departure smartpostlist if city is set
        
        if (departureBox.getValue() != null) {
            departureSmartPost.getItems().clear();
            departureSmartPost.getItems().addAll(smartposts.getSmartPosts(departureBox.getValue()));
        }
        
    }

    @FXML
    private void filterDestList(MouseEvent event) { // filters destination smartpostlist if city is set
        
        if (destinationBox.getValue() != null) {
            destinationSmartPost.getItems().clear();
            destinationSmartPost.getItems().addAll(smartposts.getSmartPosts(destinationBox.getValue()));
        }
    }

    @FXML
    private void addToItemList(ActionEvent event) { // adds user-made item to itemlist
        
        items.addItem(new Item(nameField.getText(), Double.parseDouble(sizeField.getText()), Double.parseDouble(weightField.getText()), fragileCheck.isSelected()));
        
        itemBox.getItems().clear(); // refresh the item list
        itemBox.getItems().addAll(items.getItemList());
        
        nameField.clear();
        sizeField.clear();
        weightField.clear();
        fragileCheck.setSelected(false);
        
    }

    @FXML
    private void createPackageAction(ActionEvent event) {
        
        // LISÄTÄÄN TOOL TIPIT KUN PERUSMALLI TOIMII //
        
        
        
    }
    
}
