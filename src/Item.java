
import java.util.ArrayList;

// CT60A2411 Olio-ohjelmointi
// Syksy 2016
// Harjoitustyö
// Antti Sinkkonen 0439700 & Aleksi Rantamäki 0451458
// Item-class holds the information of the items which are created by the user


public class Item {
    
    static ArrayList<Item> itemList;
    private String itemName;
    private double weight;
    private double size;
    private boolean fragile;

    public Item (String a, double b, double c, boolean p) { // builder
        itemName = a;
        weight = b;
        size = c;
        fragile = p;
    }

    public String getItemName() {
        return itemName;
    }

    public double getSize() {
        return size;
    }

    public double getWeight() {
        return weight;
    }
    
    @Override
    public String toString() {
        return itemName;
    }

    public boolean isFragile() {
        return fragile;
    }

    


} // end of class

// FOUR BASIC ITEMS


class TeddyBear extends Item {
    public TeddyBear() {
        super("Pehmeä nallekarhu", 0.4, 10*30*15, false);
    }
}

class BluetoothSpeaker extends Item {
    
    public BluetoothSpeaker() {
        super("Langaton Bluetooth-kaiutin", 1.8, 15*25*15, true);
    }
}

class WinterParka extends Item {
    
    public WinterParka() {
        super("Paksu parkatakki", 3.1, 50*90*60, false);
    }
}

class SmallMirror extends Item {

    public SmallMirror() {
        super("Pieni peili", 0.3, 20*1*30, true);
    }
} 
