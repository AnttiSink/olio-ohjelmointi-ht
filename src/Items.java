
import java.util.ArrayList;


/**
 *
 * @author Antti
 */
public class Items {
    
    private static Items instance = null;
    private ArrayList<Item> itemList = new ArrayList<>();
    
    protected Items() {
        itemList.add(new TeddyBear());
        itemList.add(new WinterParka());
        itemList.add(new SmallMirror());
        itemList.add(new BluetoothSpeaker());
        // adding 4 basic items to the list when building
    }
    
    
    public static Items getInstance() {
    if (instance == null) {
        instance = new Items();
    }
    return instance;
    }

    public ArrayList<Item> getItemList() {
        return itemList;
    }
    
    public void addItem (Item i) {
        itemList.add(i);
    }
}
