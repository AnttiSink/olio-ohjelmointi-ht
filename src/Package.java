// CT60A2411 Olio-ohjelmointi
// Syksy 2016
// Harjoitustyö
// Antti Sinkkonen 0439700 & Aleksi Rantamäki 0451458
// Package-class from which new package classes are inherited
// Contains info for the route and item inside the specific package


import java.util.ArrayList;


public class Package {
    
    private int packageClass;
    private Item item;
    private ArrayList<Double> geopoint; // Arraylist for the latitude and longitude
    private boolean fragile;
    private String departureAddress;
    private String destinationAddress;
    
    public Package(int packageClass, Item item, ArrayList<Double> geopoint, boolean f, String a, String b) { // builder
        this.packageClass = packageClass;
        this.item = item;
        this.geopoint = geopoint;
        fragile = f;
        departureAddress = a;
        destinationAddress = b;
    }

    public int getPackageClass() {
        return packageClass;
    }

    public Item getItem() {
        return item;
    }

    public ArrayList<Double> getGeopoint() {
        return geopoint;
    }

    public boolean isFragile() {
        return fragile;
    }

    public String getDepartureAddress() {
        return departureAddress;
    }

    public String getDestinationAddress() {
        return destinationAddress;
    }

    public void setPackageClass(int t) {
        t = packageClass;
    }
 
} // EOF
