// CT60A2411 Olio-ohjelmointi
// Syksy 2016
// Harjoitustyö
// Antti Sinkkonen 0439700 & Aleksi Rantamäki 0451458
// Smartposts class holds the information of all the Smartpost-objects

import java.util.ArrayList;

public class Smartposts {
    
    private static Smartposts instance = null;
    private ArrayList<Smartpost> smartpostList = new ArrayList<Smartpost>();
    private ArrayList<String> cities = new ArrayList<String>();
    
    // builder for both smartposts and  for the smartpostparser
    protected Smartposts() {
        SmartpostParser spp = new SmartpostParser();
        smartpostList = spp.getSmartposts();
        cities = spp.getCities();
    }
    
    public static Smartposts getInstance() { // Singleton
        if (instance == null) {
            instance = new Smartposts();
        }
        return instance;
    }
    
    public ArrayList<Smartpost> getSmartPosts() {
        return smartpostList;
    }

    public ArrayList<String> getCities() {
        return cities;
    }
    
    public ArrayList<Smartpost> getSmartPosts(String cityName) {
        ArrayList<Smartpost> list = new ArrayList();
        
        for (int j = 0; j < smartpostList.size(); j++) {
            if (smartpostList.get(j).getCity().equals(cityName)) {
                list.add(smartpostList.get(j));
            }
        }
      
        return list;
    } 
    
} // EOF
