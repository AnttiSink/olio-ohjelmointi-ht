// CT60A2411 Olio-ohjelmointi
// Syksy 2016
// Harjoitustyö
// Antti Sinkkonen 0439700 & Aleksi Rantamäki 0451458
// Storage-class is used for storing previously created packages


import java.util.ArrayList;



public class Storage {
    
    private static Storage instance = null;
    private ArrayList<Package> packageList;

    protected Storage() {
        packageList = new ArrayList<Package>();
    }
    
    public static Storage getInstance() {
        if (instance == null) {
            instance = new Storage();
        }
        return instance;
    }
    
    public void addPacket(Package p) {
        packageList.add(p);
    }
    
    public ArrayList<Package> getStorage() {
        return packageList;
    }
    
}
